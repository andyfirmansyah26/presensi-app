# Mata Kuliah / Kelas
Implementasi dan Evaluasi Sistem Informasi / Sistem Informasi - G

## Dosen Pengampu 
Djoko Pramono, S.T., M.Kom.

## Kelompok 4
- Andy Firmansyah (205150401111047)
- Fatih Daffa Nurma S. (205150400111035)
- M. Nauval Al-Aizar R. (205150400111022)
___
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Tentang Project Ini
PresensiApp merupakan aplikasi yang dikembangkan dalam rangka memenuhi penugasan dari mata kuliah Implementasi dan Evaluasi Sistem Informasi.

## _Tech stacks_ yang digunakan
- Laravel 9 <a href="https://cdnlogo.com/logo/laravel_40129.html"><img src="https://cdn.cdnlogo.com/logos/l/57/laravel.svg" height="16"></a>
- Bootstrap 5.2 <a href="https://cdnlogo.com/logo/bootstrap-5_40714.html"><img src="https://cdn.cdnlogo.com/logos/b/74/bootstrap-5.svg" height="16"></a>
- jQuery <a href="https://cdnlogo.com/logo/jquery_39697.html"><img src="https://cdn.cdnlogo.com/logos/j/45/jquery.svg" height="16"></a>
- SBAdmin2 (Dashboard Admin template)

## What to do?
Untuk dapat melihat aplikasi ini, Anda perlu melakukan clone repository ini terlebih dahulu.

    git clone https://gitlab.com/andyfirmansyah26/presensi-app.git
    
Setelah itu, lakukan konfigurasi pada file `.env.example`, dengan me-<em>rename</em> file tersebut menjadi `.env`, dan sesuaikan konfigurasinya dengan <em>database server</em> Anda.

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=presensi_app
    DB_USERNAME=root
    DB_PASSWORD=

Kemudian, Anda perlu menjalankan perintah berikut pada folder _root_ aplikasi untuk melakukan _migration_ dan _seeding_ pada _database_. Sebelum itu, Anda perlu melakukan _generate key_ pada project ini. Anda juga perlu meng-_install_ composer agar bisa melakukan _migration_ & _seeding_. Pastikan Anda memiliki server MYSQL yang aktif pada _local computer_ Anda.

    cd presensi-app
    
    composer install

    php artisan key:generate
    
    php artisan migrate:fresh --seed

Setelah melakukan semua proses di atas, maka aplikasi PresensiApp siap digunakan.

## Dummy User
- Role `student`

    Email: student[at]gmail.com

    Password: password

- Role `admin`

    Email: admin[at]gmail.com

    Password: password
